# templater

Specific util to generate  docx documents from csv data (view some samples in samples/)

# Download

You can find binaries in binaries/ folder

# Usage

```
    ./processor(.exe/.app) --input input.csv --output output.zip
```