package buildtools

import (
	"io/ioutil"
	"os"
	"strconv"

	s "gitlab.com/Ivanchgeek/templater-back/structs"
	u "gitlab.com/Ivanchgeek/templater-back/utl"
)

var files = []s.FileToZip{{Path: "buildtools/docx_satellites/[Content_Types].xml", Name: "docx_satellites/[Content_Types].xml"},
	{Path: "buildtools/docx_satellites/_rels/.rels", Name: "docx_satellites/_rels/.rels"},
	{Path: "buildtools/docx_satellites/customXML/item1.xml", Name: "docx_satellites/customXML/item1.xml"},
	{Path: "buildtools/docx_satellites/customXML/itemProps1.xml", Name: "docx_satellites/customXML/itemProps1.xml"},
	{Path: "buildtools/docx_satellites/customXML/_rels/item1.xml.rels", Name: "docx_satellites/customXML/_rels/item1.xml.rels"},
	{Path: "buildtools/docx_satellites/docProps/core.xml", Name: "docx_satellites/docProps/core.xml"},
	{Path: "buildtools/docx_satellites/docProps/custom.xml", Name: "docx_satellites/docProps/custom.xml"},
	{Path: "buildtools/docx_satellites/docProps/custom.xml", Name: "docx_satellites/word/document.xml"},
	{Path: "buildtools/docx_satellites/word/fontTable.xml", Name: "docx_satellites/word/fontTable.xml"},
	{Path: "buildtools/docx_satellites/word/numbering.xml", Name: "docx_satellites/word/numbering.xml"},
	{Path: "buildtools/docx_satellites/word/settings.xml", Name: "docx_satellites/word/settings.xml"},
	{Path: "buildtools/docx_satellites/word/styles.xml", Name: "docx_satellites/word/styles.xml"},
	{Path: "buildtools/docx_satellites/word/_rels/document.xml.rels", Name: "docx_satellites/word/_rels/document.xml.rels"},
	{Path: "buildtools/docx_satellites/word/theme/theme1.xml", Name: "docx_satellites/word/theme/theme1.xml"},
	{Path: "buildtools/templates/templ_doc.xml", Name: "templates/templ_doc.xml"},
	{Path: "buildtools/templates/templ_row.xml", Name: "templates/templ_row.xml"}}

//GenerateDataBytesDotGo generate data_bytes.go from app's data in docx folder
func GenerateDataBytesDotGo() error {
	err := u.ZipFiles("buildtools/data.zip", files)
	defer os.Remove("buildtools/data.zip")
	if err != nil {
		return err
	}

	data, err := ioutil.ReadFile("buildtools/data.zip")
	if err != nil {
		return err
	}

	var outputBytes []byte
	outputBytes = append(outputBytes, []byte("package data\n\nvar (\ndata = []byte{")...)
	for count, bt := range data {
		outputBytes = append(outputBytes, []byte(strconv.Itoa(int(bt)))...)
		if count != len(outputBytes) {
			outputBytes = append(outputBytes, []byte(",")...)
		}
	}
	outputBytes = append(outputBytes, []byte("}\n)")...)

	os.Remove("data/data_bytes.go")

	err = ioutil.WriteFile("data/data_bytes.go", outputBytes, os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}
