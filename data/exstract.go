package data

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/cheggaaa/pb"
	u "gitlab.com/Ivanchgeek/templater-back/utl"
)

//PrepareApp - exstract app files and create workspace folder
func PrepareApp() {
	fmt.Println("exstracting app data")
	err := os.Mkdir("workspace", os.ModePerm)
	if err != nil {
		fmt.Println("try to remove this folders: \n*docx_satellites\n*templates\n*workspace\n\nand try again")
		u.ErrorAlert("can't create app's work folder")
	}
	zip, err := os.Create("data.zip")
	defer os.Remove("data.zip")
	if err != nil {
		u.ErrorAlert("can't create zip file to exstract data")
	}
	fmt.Println("Data recovering")
	recBar := pb.StartNew(len(data))
	zipWriter := bufio.NewWriter(zip)
	for _, b := range data {
		err := zipWriter.WriteByte(b)
		if err != nil {
			u.ErrorAlert("crush exstracting app data")
		}
		err = zipWriter.Flush()
		if err != nil {
			u.ErrorAlert("crush exstracting app data")
		}
		recBar.Increment()
	}
	err = zip.Close()
	if err != nil {
		u.ErrorAlert("crush exstracting app data")
	}
	recBar.Finish()
	fmt.Println("Unzipping data")
	err = unzip("data.zip", "")
	if err != nil {
		u.ErrorAlert("crush exstracting app data")
	}
}

//ClearWorkspace - remove app data
func ClearWorkspace() {
	err := os.RemoveAll("templates")
	err = os.RemoveAll("docx_satellites")
	err = os.RemoveAll("workspace")
	if err != nil {
		u.ErrorAlert("can't clear app data")
	}
	fmt.Println("app's data cleared")
}

func unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	ubar := pb.StartNew(len(r.File))
	for _, f := range r.File {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer rc.Close()

		fpath := filepath.Join(dest, f.Name)
		if f.FileInfo().IsDir() {
			os.MkdirAll(fpath, f.Mode())
		} else {
			var fdir string
			if lastIndex := strings.LastIndex(fpath, string(os.PathSeparator)); lastIndex > -1 {
				fdir = fpath[:lastIndex]
			}

			err = os.MkdirAll(fdir, f.Mode())
			if err != nil {
				log.Fatal(err)
				return err
			}
			f, err := os.OpenFile(
				fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer f.Close()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		ubar.Increment()
	}
	ubar.Finish()
	return nil
}
