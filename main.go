package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/cheggaaa/pb"
	"github.com/common-nighthawk/go-figure"
	"gitlab.com/Ivanchgeek/templater-back/buildtools"
	"gitlab.com/Ivanchgeek/templater-back/data"
	s "gitlab.com/Ivanchgeek/templater-back/structs"
	u "gitlab.com/Ivanchgeek/templater-back/utl"
)

var (
	input  string
	output string
	build  bool
)

func init() {
	flag.StringVar(&input, "input", "input.csv", "input data file's name")
	flag.StringVar(&output, "output", "output.zip", "output zip's name")
	flag.BoolVar(&build, "build", false, "codegen to update app's data")
	flag.Parse()
}

func main() {
	if build {
		err := buildtools.GenerateDataBytesDotGo()
		if err != nil {
			fmt.Println("error while building")
			panic(err)
		}
		fmt.Println("builded")
		return
	}

	bye := figure.NewFigure("Bye :)", "", true)
	defer bye.Print()

	fmt.Println("https://gitlab.com/Ivanchgeek/templater-back")
	fmt.Println("mail: iam@ivanchgeek.ru")
	fmt.Println("---------------")

	data.PrepareApp()
	defer data.ClearWorkspace()

	documentsData := u.ParseCSV(input)

	files := []s.FileToZip{}
	fmt.Println("Generating files: ")
	bar := pb.StartNew(len(documentsData))
	for _, data := range documentsData {
		fileName := u.GenerateDocument(data)
		files = append(files, s.FileToZip{Path: fileName, Name: data.Building + "_" + data.Name + ".docx"})
		defer os.Remove(fileName)
		bar.Increment()
	}
	bar.Finish()

	fmt.Println("Zipping result")
	err := u.ZipFiles(output, files)
	if err != nil {
		u.ErrorAlert("error while zipping result")
	}
}
