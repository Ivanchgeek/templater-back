package structs

//FileToZip - info to correctly zip (add to docx) file
type FileToZip struct {
	Path string
	Name string
}

//Row - table row
type Row struct {
	GroupName string
	Hours     string
	Week      string
	HoursWork string
}

//TemplateInput - input variables for template
type TemplateInput struct {
	Building  string
	Teacher   string
	Name      string
	DateStart string
	DateEnd   string
	Rows      []Row
}
