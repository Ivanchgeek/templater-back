package utl

import (
	"archive/zip"
	"bufio"
	"crypto/rand"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	s "gitlab.com/Ivanchgeek/templater-back/structs"
)

//https://golangcode.com/create-zip-files-in-go/  THANK YOU:)

// ZipFiles compresses one or many files into a single zip archive file.
// Param 1: filename is the output zip file's name.
// Param 2: files is a list of files to add to the zip.
func ZipFiles(filename string, files []s.FileToZip) error {
	newZipFile, err := os.Create("./" + filename)
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	for _, file := range files {
		if err = AddFileToZip(zipWriter, file); err != nil {
			return err
		}
	}
	return nil
}

//AddFileToZip add file
func AddFileToZip(zipWriter *zip.Writer, filename s.FileToZip) error {
	fileToZip, err := os.Open("./" + filename.Path)
	if err != nil {
		return err
	}
	defer fileToZip.Close()
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}
	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}
	header.Name = filename.Name
	header.Method = zip.Deflate
	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}

//ProcessTemplates - generate document.xml
func ProcessTemplates(name string, input s.TemplateInput) string {
	var outputName = name + "_processed.xml"

	outputFile, err := os.Create(outputName)
	if err != nil {
		panic(err)
	}
	defer outputFile.Close()

	outputWriter := bufio.NewWriter(outputFile)

	DocumentTemplate, err := os.Open("templates/templ_doc.xml")
	if err != nil {
		panic(err)
	}
	defer DocumentTemplate.Close()
	DocumentTemplateScanner := bufio.NewScanner(DocumentTemplate)

	for DocumentTemplateScanner.Scan() {
		line := DocumentTemplateScanner.Text()

		switch line {
		case "$BUILDING":
			outputWriter.WriteString(input.Building)
		case "$TEACHER":
			outputWriter.WriteString(input.Teacher)
		case "$NAME":
			outputWriter.WriteString(input.Name)
		case "$DATE_START":
			outputWriter.WriteString(input.DateStart)
		case "$DATE_END":
			outputWriter.WriteString(input.DateEnd)
		case "$ROWS":
			for _, row := range input.Rows {

				RowTemplate, err := os.Open("templates/templ_row.xml")
				if err != nil {
					log.Println(err)
				}
				defer RowTemplate.Close()
				RowTemplateScanner := bufio.NewScanner(RowTemplate)

				for RowTemplateScanner.Scan() {
					RowTemplateLine := RowTemplateScanner.Text()

					switch RowTemplateLine {
					case "$GROUP_NAME":
						outputWriter.WriteString(row.GroupName)
					case "$HOURS":
						outputWriter.WriteString(row.Hours)
					case "$WEEK":
						outputWriter.WriteString(row.Week)
					case "$HOURS_WORK":
						outputWriter.WriteString(row.HoursWork)
					default:
						outputWriter.WriteString(RowTemplateLine)
					}

					outputWriter.Flush()
				}

			}
		default:
			outputWriter.WriteString(line)
		}

		outputWriter.Flush()
	}

	if err := DocumentTemplateScanner.Err(); err != nil {
		panic(err)
	}

	return outputName
}

//GenerateDocument - generate docx
func GenerateDocument(input s.TemplateInput) string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	var zipName = "workspace/" + fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:]) + ".docx"

	var documentFile = ProcessTemplates(zipName, input)
	defer os.Remove(documentFile)

	var files = []s.FileToZip{{Path: "docx_satellites/[Content_Types].xml", Name: "[Content_Types].xml"},
		{Path: "docx_satellites/_rels/.rels", Name: "_rels/.rels"},
		{Path: "docx_satellites/customXML/item1.xml", Name: "customXML/item1.xml"},
		{Path: "docx_satellites/customXML/itemProps1.xml", Name: "customXML/itemProps1.xml"},
		{Path: "docx_satellites/customXML/_rels/item1.xml.rels", Name: "customXML/_rels/item1.xml.rels"},
		{Path: "docx_satellites/docProps/core.xml", Name: "docProps/core.xml"},
		{Path: "docx_satellites/docProps/custom.xml", Name: "docProps/custom.xml"},
		{Path: documentFile, Name: "word/document.xml"},
		{Path: "docx_satellites/word/fontTable.xml", Name: "word/fontTable.xml"},
		{Path: "docx_satellites/word/numbering.xml", Name: "word/numbering.xml"},
		{Path: "docx_satellites/word/settings.xml", Name: "word/settings.xml"},
		{Path: "docx_satellites/word/styles.xml", Name: "word/styles.xml"},
		{Path: "docx_satellites/word/_rels/document.xml.rels", Name: "word/_rels/document.xml.rels"},
		{Path: "docx_satellites/word/theme/theme1.xml", Name: "word/theme/theme1.xml"}}

	err = ZipFiles(zipName, files)
	if err != nil {
		panic(err)
	}

	return zipName
}

//ParseCSV - transfrom tabled data to docx input struct
func ParseCSV(file string) []s.TemplateInput {
	CSVFile, err := os.Open(file)
	if err != nil {
		ErrorAlert("Can't find file: " + file)
	}
	defer CSVFile.Close()
	lines, err := csv.NewReader(CSVFile).ReadAll()
	if err != nil {
		ErrorAlert("Can't read file: " + file)
	}

	var userData = map[string]s.TemplateInput{}

	for i, row := range lines {
		if i == 0 {
			continue
		}
		if len(row) != 9 {
			ErrorAlert("Wrong csv input format, view example on https://gitlab.com/Ivanchgeek/templater-back/blob/master/test.csv \n(wrong row length in line" + strconv.Itoa(i) + ")")
		}
		parseRow := s.TemplateInput{
			Building:  row[0],
			Teacher:   row[1],
			Name:      row[2],
			DateStart: row[3],
			DateEnd:   row[4],
			Rows: append(userData[row[0]+row[2]].Rows,
				s.Row{
					GroupName: row[5],
					Hours:     row[6],
					Week:      row[7],
					HoursWork: row[8]})}
		userData[row[0]+row[2]] = parseRow
	}

	result := []s.TemplateInput{}

	for _, data := range userData {
		result = append(result, data)
	}

	return result
}

//ErrorAlert - alert user about error
func ErrorAlert(e string) {
	fmt.Println("------------------------")
	fmt.Println(e)
	fmt.Println("------------------------")
	err := os.RemoveAll("templates")
	err = os.RemoveAll("docx_satellites")
	err = os.RemoveAll("workspace")
	if err != nil {
		fmt.Println("---ERROR CLEANING APP DATA----\nplease manualy remove this folders: \n*docx_satellites\n*templates\n*workspace")
	}
	fmt.Println("app's data cleared")
	os.Exit(1)
}
